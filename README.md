## Dependencies
* Python 3.6.10
* opencv-python==3.4.2.16
* opencv-contrib-python==3.4.2.16
* numpy==1.18.1
* scikit-image==0.16.2
* scipy==1.4.1

The full list of dependencies can be found in `requirements.txt`. There might be
some leftover dependencies in `requirements.txt` from `simpleaudio` and
`pytesseract`. These dependencies are no longer used in this project.

## Running the project
Script for all scenarios: `proj1.py`

Function for all scenarios: create a `Solver` object and call the `solve` method

Output: results are written to `results/micluta_campeanu_marius_407_task?.txt`,
where `?` is 1, 2, or 3, depending on which task is run.

With the right permissions (`chmod +x proj1.py` on Unix-like systems),
you can replace `python proj1.py ...` with `./proj1.py ...`
### Scenario 1
```sh
python proj1.py --task=1 \
                --images_folder=additional_data/1.scanned/ \
                --labels_in_file_name \
                --output_folder=results/ \
                --gt_folder=ground-truth/ \
                --internal_folder=internal/ \
                --needs_transform=1 \
                --images_pattern=*.jpg
```

By default, results are written in a folder called `results` (which is created
if it does not exist), the image filename pattern is `*.jpg`. If the internal
files are also found in a folder called `internal` and the ground truth labels
are in a folder called `ground-truth`, then the command can be simplified to
```sh
python proj1.py --task=1 \
                --images_folder=additional_data/1.scanned/ \
                --labels_in_file_name \
                --needs_transform=1
```

### Scenario 2

```sh
python proj1.py --task=2 \
                --images_folder=additional_data/2.rotated+perspective/ \
                --labels_in_file_name \
                --output_folder=results/ \
                --gt_folder=ground-truth/ \
                --internal_folder=internal/ \
                --needs_transform=2
```
### Scenario 3

```sh
python proj1.py --task=3 \
                --images_folder=additional_data/3.no_annotation/ \
                --output_folder=results/ \
                --gt_folder=ground-truth/ \
                --internal_folder=internal/ \
                --needs_transform=0
```
### 
