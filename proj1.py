#!/usr/bin/env python3
# Computer vision
# Automatic grading of multiple choice tests

import glob
import os

import cv2 as cv
import numpy as np

from argparse import ArgumentParser
from typing import Union

from skimage.metrics import structural_similarity


def add_lines_to_image(color_image, v_correct_lines):
    for line in v_correct_lines:
        cv.line(color_image, line[0], line[1], (255, 0, 0), 2)


class Solver(object):
    def __init__(self, images_folder: str = os.getcwd(), gt_folder: str = None, images_pattern: str = '*.jpg',
                 has_labels: bool = False, labels_in_file_name: bool = False,
                 needs_transform: Union[bool, None] = None, internal_folder: str = os.getcwd(),
                 digit_pos=0) -> None:
        super().__init__()
        self.LABELS_IN_FILE_NAME = labels_in_file_name
        self.gt = {}
        self.SHOW_INTERMEDIATE_RESULTS = False  # set this to false in order to show only the final result
        self.NUM_OF_SECONDS = 0  # set to 0 in order to wait until keypress

        self.VERBOSE = False
        self.HAS_LABELS = has_labels
        self.internal_folder = internal_folder
        self.check_file_exists(internal_folder, "Internal folder")
        header_path = os.path.join(internal_folder, 'header2.jpg')
        self.check_file_exists(header_path, "Header file")
        header2 = cv.imread(header_path)
        self.header2 = cv.cvtColor(header2, cv.COLOR_BGR2GRAY)

        self.needs_transform = self.apply_transform = needs_transform
        if needs_transform is None and self.VERBOSE:
            print("[Auto-detect] Scenario detection enabled")
        if self.apply_transform is True:
            self.init_template()
        else:
            self.img_template = None

        # read the name of the images
        self.base_folder = images_folder
        self.gt_folder = gt_folder
        self.images_names = glob.glob(os.path.join(self.base_folder, images_pattern))
        if len(self.images_names) is 0:
            print("[WARNING] No images found")
        self.images_names.sort(
          key=lambda x: int(x.split('/')[-1].split('_')[digit_pos].split('.')[0])
        )

        self.init_ground_truth()

    @staticmethod
    def check_file_exists(file_name, message):
        if not os.path.exists(file_name):
            raise FileNotFoundError(f"{message} {file_name} not found")

    def init_template(self):
        template_path = os.path.join(self.internal_folder, 'template_both.jpg')
        self.check_file_exists(template_path, "Template file")
        self.img_template = cv.imread(template_path)
        self.img_template = cv.cvtColor(self.img_template, cv.COLOR_BGR2RGB)

    def show_image(self, image):
        if self.SHOW_INTERMEDIATE_RESULTS:
            cv.imshow("image", image)
            cv.waitKey(self.NUM_OF_SECONDS)
            cv.destroyAllWindows()

    def solve(self, start=0, intermediate_results=False):
        scores = []
        wrong = 0
        self.SHOW_INTERMEDIATE_RESULTS = intermediate_results
        for idx_image in range(start, len(self.images_names)):
            print("Detecting edges on the %d image..." % idx_image)

            image = cv.imread(self.images_names[idx_image])

            if self.needs_transform is None:
                s2 = self.compute_similarity(image, self.header2)
                if s2 < 0.5855:
                    self.apply_transform = True
                else:
                    if self.VERBOSE:
                        print("[Auto-detect] No transformation needed")
            if self.apply_transform:
                if self.img_template is None:
                    self.init_template()
                image = self.transform_image(image)
                img1_gray = self.convert_to_grayscale(image, needs_resize=False,
                                                      crop_first_half=False, crop_bottom=True)
            else:
                img1_gray = self.convert_to_grayscale(image)
            img1_th = self.apply_threshold(img1_gray)

            correct_lines, v_correct_lines = self.find_grid(img1_gray, img1_th)

            labels = []
            real_score = None

            if self.HAS_LABELS:
                _, _, real_score, _ = self.read_labels(idx_image)
                nr, variant = None, None
                # labels, nr, real_score, variant = self.read_labels(idx_image)
            elif self.LABELS_IN_FILE_NAME:
                label = self.images_names[idx_image][:-4]
                variant = label[-2]
                nr = int(label[-1])
            else:
                variant = None
                nr = None

            l_correction = 5 if self.apply_transform else -2
            r_correction = 5 if self.apply_transform else 0
            y_correction = 2 if self.apply_transform else 0
            answers = self.grade_column(correct_lines, v_correct_lines, img1_th, idx_image, labels,
                                        vertical_start=1, vertical_stop=5, offset=0, eps=3, correction=l_correction)
            answers += self.grade_column(correct_lines, v_correct_lines, img1_th, idx_image, labels,
                                         vertical_start=6, vertical_stop=10, offset=15,
                                         eps=3, correction=r_correction, y_correction=y_correction)

            score, var, nmb = self.score(answers, variant, nr, img1_gray, v_correct_lines)
            if real_score is not None and score != real_score:
                print(f"Error, expected {real_score}, got {score}!")
                wrong += 1
            final_score = score * 0.3 + 1
            file_name = self.images_names[idx_image].split('/')[-1]
            print(f"{file_name}: got {final_score:.2f} for {var}{nmb}")
            scores.append(f"{file_name}\t{final_score:.2f}")  # \t{var}\t{nmb}")
        return scores, wrong

    def read_labels(self, idx_image):
        labels_names = glob.glob(os.path.join(self.base_folder, "image_*.txt"))
        labels_names.sort(key=lambda x: int(x.split('/')[-1].split('_')[-1].split('.')[0]))
        with open(labels_names[idx_image], "r") as f:
            labels = f.readlines()
        variant, nr = labels[0].split()
        real_score = int(labels[-1].split()[1])
        return labels, nr, real_score, variant

    def apply_threshold(self, img1_gray):
        # obtain a binary image by applying an adaptive threshold = mean value of img1_gray
        threshold = 45 if self.apply_transform else 0
        mean_value = np.mean(img1_gray) - threshold
        _, img1_th = cv.threshold(img1_gray, mean_value, 255,
                                  cv.THRESH_BINARY_INV)  # the second params is the threshold
        self.show_image(img1_th)
        return img1_th

    def find_grid(self, img1_gray, img1_th):
        # define a vertical kernel and filter the binary image
        kernel = np.array([
            [0, 1, 0, 0],
            [0, 1, 0, 0],
            [0, 1, 0, 0],
            [0, 1, 0, 0],
            [0, 1, 0, 0]
        ])
        filtered_vertical = self.filter_with_kernel(img1_th, kernel)
        self.show_image(filtered_vertical)
        # define a horizontal kernel and filter the binary image
        h_kernel = np.array([
            [0, 0, 0],
            [1, 1, 1],
            [0, 0, 0]
        ])
        filtered_horizontal = self.filter_with_kernel(img1_th, h_kernel)
        self.show_image(filtered_horizontal)
        # find horizontal lines
        color_image, correct_lines = self.find_horizontal_lines(filtered_horizontal, img1_gray)
        self.show_image(color_image)
        # find vertical lines
        color_image, v_correct_lines = self.find_vertical_lines(filtered_vertical, img1_gray)
        self.show_image(color_image)
        return correct_lines, v_correct_lines

    def find_vertical_lines(self, filtered_vertical, img1_gray):
        v_res = self.axis_mask(filtered_vertical, 0)

        v_num_lines = 180 if self.apply_transform else 40
        v_img = np.dstack((filtered_vertical, filtered_vertical, filtered_vertical))
        v_lines = []
        for i in range(v_num_lines + 1):
            cv.line(v_img, (v_res[-i], 0), (v_res[-i], v_img.shape[0]), (0, 0, 255), 2)
            v_lines.append([(v_res[-i], 0), (v_res[-i], v_img.shape[0])])
        self.show_image(v_img)
        # sort only on x0 !!! only
        v_lines.sort(key=lambda coords: coords[0][0])
        threshold_same_line = 25 if self.apply_transform else 20  # nr of pixels between lines
        threshold_v = 123 if self.apply_transform else 225  # nr of pixels between lines
        distinct_lines = [[line for line in v_lines if line[0][0] > threshold_v][0]]
        for line in v_lines:
            if threshold_same_line < line[0][0] - distinct_lines[-1][0][0]:
                distinct_lines.append(line)

        q = self.count_left_extra_lines(distinct_lines)
        v_correct_lines = [distinct_lines[q]]
        for i in range(4):
            v_correct_lines.append([(v_correct_lines[-1][0][0] + 35,
                                     v_correct_lines[-1][0][1]),
                                    (v_correct_lines[-1][1][0] + 35,
                                     v_correct_lines[-1][1][1])])
        color_image = np.dstack((img1_gray, img1_gray, img1_gray))
        add_lines_to_image(color_image, v_correct_lines)
        self.show_image(color_image)
        threshold = 665 if self.apply_transform else 770
        right_lines = [[line for line in v_lines if line[0][0] > threshold][0]]
        for i in range(4):
            right_lines.append([(right_lines[-1][0][0] + 35,
                                 right_lines[-1][0][1]),
                                (right_lines[-1][1][0] + 35,
                                 right_lines[-1][1][1])])
        # right_lines.reverse()
        v_correct_lines += right_lines
        color_image = np.dstack((img1_gray, img1_gray, img1_gray))
        add_lines_to_image(color_image, v_correct_lines)
        return color_image, v_correct_lines

    def count_left_extra_lines(self, distinct_lines):
        k = 0
        q = 0
        for i in range(1, len(distinct_lines)):
            if self.VERBOSE:
                print(distinct_lines[i][0][0] - distinct_lines[i - 1][0][0])
            if distinct_lines[i][0][0] - distinct_lines[i - 1][0][0] > 60:
                q += 1
                if self.VERBOSE:
                    print("r", i - 1)
            else:
                k += 1
            if k == 4:
                break
        return q

    def find_horizontal_lines(self, filtered_horizontal, img1_gray, nr=16):
        res = self.axis_mask(filtered_horizontal, 1)

        num_lines = 130 if nr == 16 else 30
        h_img = np.dstack((filtered_horizontal, filtered_horizontal, filtered_horizontal))
        lines = []
        for i in range(num_lines + 1):
            cv.line(h_img, (0, res[-i]), (h_img.shape[1], res[-i]), (0, 0, 255), 2)
            lines.append([(0, res[-i]), (h_img.shape[1], res[-i])])
        self.show_image(h_img)
        # sort only on y0 !!! only
        lines.sort(key=lambda coords: coords[0][1])
        threshold_same_line = 21 if self.apply_transform else 24  # nr of pixels between lines
        thr = 172 if nr == 16 else 0
        first_line = [line for line in lines if line[0][1] > thr][0]

        distinct_lines = [first_line]
        for line in lines:
            if line[0][1] - distinct_lines[-1][0][1] > threshold_same_line:
                distinct_lines.append(line)
        # take the last 16 lines
        if distinct_lines[-1][0][1] > h_img.shape[0] - 5:
            correct_lines = distinct_lines[-nr - 1:-1]
        else:
            correct_lines = distinct_lines[-nr:]
        color_image = np.dstack((img1_gray, img1_gray, img1_gray))
        add_lines_to_image(color_image, correct_lines)
        return color_image, correct_lines

    def axis_mask(self, filtered_horizontal, axis):
        mask = filtered_horizontal // 255
        if self.VERBOSE:
            print(mask.min())
            print(mask.max())
        res = np.sum(mask, axis=axis)
        if self.VERBOSE:
            print(res.shape)
            print(res)
        res = res.argsort()
        return res

    def filter_with_kernel(self, img1_th, kernel):
        if self.VERBOSE:
            print(kernel.sum())
        kernel = kernel / kernel.sum()
        if self.VERBOSE:
            print(kernel)
            print(kernel.sum())
        filtered = 255 - cv.filter2D(255 - img1_th, -1, kernel)
        filtered[filtered < 255] = 0
        filtered_vertical = filtered
        return filtered_vertical

    def convert_to_grayscale(self, image, needs_resize=True, crop_first_half=True, crop_bottom=False):
        if needs_resize:
            # make it 0.25 times on every dimension
            img = cv.resize(image, (0, 0), fx=0.25, fy=0.25)
            if self.VERBOSE:
                print(image.shape)
                print(img.shape)
                self.show_image(img)
        else:
            img = image
        orig_h, orig_w, _ = img.shape

        if crop_first_half:
            img1 = img[orig_h // 2 - 90:orig_h - 170, :]
        elif crop_bottom:
            img1 = img[110:orig_h - 100, 95:orig_w - 65]
        else:
            img1 = img

        return cv.cvtColor(img1, cv.COLOR_RGB2GRAY)

    def grade_column(self, correct_lines, v_correct_lines,
                     img1_th, idx_image, labels,
                     vertical_start, vertical_stop, offset,
                     eps=2, correction=0, y_correction=0):
        letters = ['A', 'B', 'C', 'D']
        answers = []

        for i in range(1, len(correct_lines)):
            # answer from math, 4 possibilities
            x1 = correct_lines[i - 1][0][1] + eps + correction + eps * (i == 0)
            x2 = correct_lines[i][0][1] - eps
            answer_value = 0
            answer_letter = 'A'
            for j in range(vertical_start, vertical_stop):
                y1 = v_correct_lines[j - 1][0][0] + eps + 3 + y_correction * (j - vertical_start) + (5 - j) * (j < 5)
                y2 = v_correct_lines[j][0][0] - eps + y_correction * (j - vertical_start) // 2
                # print(x1, y1, x2, y2)
                if x1 + 5 >= x2:
                    m = (x1 - x2)
                    x2 += m + correction
                    x1 -= 2 * (m + correction)
                if y1 >= y2:
                    m = (y1 - y2) / 2
                    y1 -= m
                    y2 += m
                # print(x1, y1, x2, y2)
                mean = np.mean(img1_th[x1:x2, y1:y2])
                # img1_th[x1:x2, y1:y2]=255
                # self.show_image(img1_th)
                # print(mean)
                if mean > answer_value:
                    answer_value = mean
                    answer_letter = letters[j - vertical_stop + 5 - 1]

            answers.append(answer_letter)
            if self.HAS_LABELS and len(labels) > 0:
                if labels[i + offset].split()[1] != answer_letter:
                    print(f"Error {self.images_names[idx_image]} {i + offset}:"
                          f"expected {labels[i + offset].split()[1]}, got {answer_letter}")
            else:
                pass
                # print(f"{i + offset}: Got {answer_letter}")
        # self.show_image(img1_th)
        return answers

    def score(self, answers, name, variant, image, v_lines):
        score = 0
        if self.gt_folder is not None:
            if name is not None and variant is not None:
                for answer, gold_answer in zip(answers, self.gt[f"{name}_{variant}"][1:]):
                    if answer == gold_answer[-2]:
                        score += 1
            else:
                mean_value = np.mean(image) - 50
                _, img = cv.threshold(image, mean_value, 255,
                                      cv.THRESH_BINARY_INV)
                h, w = img.shape
                h1 = 0 if self.apply_transform else 15
                h2 = h // 5 if self.apply_transform else h // 5 + 5
                # w2 = 0 if self.apply_transform else 70
                # w1 = 7 * w // 8 if self.apply_transform else 6 * w // 7 - 35
                # corner = img[h1:h2, w1:w - w2]
                w1 = v_lines[-2][0][0] + 8 if self.apply_transform else 6 * w // 7 - 35
                w2 = v_lines[-1][0][0] + 2 if self.apply_transform else w - 70
                corner = img[h1:h2, w1:w2]
                h_kernel = np.array([
                    [0, 0, 0],
                    [1, 1, 1],
                    [0, 0, 0]
                ])
                filtered_horizontal = self.filter_with_kernel(corner, h_kernel)
                # find horizontal lines
                _, correct_lines = self.find_horizontal_lines(filtered_horizontal, corner, 4)
                # color_image, correct_lines = self.find_horizontal_lines(filtered_horizontal, corner, 4)
                # self.SHOW_INTERMEDIATE_RESULTS = True
                # print(correct_lines)
                # self.show_image(color_image)
                # self.SHOW_INTERMEDIATE_RESULTS = False
                if len(correct_lines) >= 4:
                    y1 = correct_lines[0][0][1]
                    y2 = correct_lines[1][0][1]
                    y3 = correct_lines[2][0][1]
                    y4 = correct_lines[3][0][1]
                    csi = corner[y1 + 3:y2, :]
                    cs = np.mean(csi)
                    ph = corner[y3 + 3:y4, :]
                    physics = np.mean(ph)
                else:
                    cs = np.mean(corner[:h2 // 2, :])
                    physics = np.mean(corner[h2 // 2:, :])

                if cs > physics:
                    n = 'I'
                else:
                    n = 'F'
                for var in range(1, 5):
                    c_score = 0
                    for answer, gold_answer in zip(answers, self.gt[f"{n}_{var}"][1:]):
                        if answer == gold_answer[-2]:
                            c_score += 1
                    if score < c_score:
                        score = c_score
                        variant = var
                        name = n

        return score, name, variant

    def init_ground_truth(self):
        if self.gt_folder is not None:
            self.check_file_exists(self.gt_folder, "Ground truth folder")
            files = glob.glob(os.path.join(self.gt_folder, r"*_varianta*.txt"))
            if len(files) is 0:
                raise FileNotFoundError("No ground truth file found")
            for file in files:
                name, variant = file.split('/')[-1][:-4].split('_')
                name = name[0]
                variant = variant[-1]
                with open(file, "r") as f:
                    self.gt[f"{name}_{variant}"] = f.readlines()

    def transform_image(self, image):
        image2 = cv.cvtColor(image, cv.COLOR_BGR2RGB)
        image2 = cv.resize(image2, (0, 0), fx=0.86, fy=0.86)
        orig_h, orig_w, _ = image.shape
        h2s = range(313, 471, 50)
        for h2 in h2s:
            if self.VERBOSE:
                print(h2)
            image = image2[orig_h // 3 - 451: 2 * orig_h // 3 + h2, orig_w // 6 - 120: orig_w - 215, :]

            rem = False
            img4 = cv.cvtColor(image, cv.COLOR_RGB2GRAY)
            mean_value = np.mean(img4) - 15
            _, img4t = cv.threshold(img4, mean_value, 255,
                                    cv.THRESH_BINARY_INV)
            kernel = np.ones(75)
            kernel = kernel / kernel.sum()
            filtered = 255 - cv.filter2D(255 - img4t, -1, kernel)
            filtered[filtered < 255] = 0
            coords = np.argwhere(filtered[0:100, :] > 0)
            if len(coords) > 0:
                image = image2[orig_h // 3 - 450 + 80: 2 * orig_h // 3 + h2 + 96, orig_w // 6 - 100: orig_w - 210, :]
                if self.VERBOSE:
                    print("remove black square")
                rem = True

            _, img4t = cv.threshold(img4, mean_value, 255,
                                    cv.THRESH_BINARY)
            kernel = np.ones(80)
            kernel = kernel / kernel.sum()
            filtered = 255 - cv.filter2D(255 - img4t, -1, kernel)
            filtered[filtered < 255] = 0
            coords = np.argwhere(filtered[-200:, :] > 0)
            if not rem and len(coords) > 0:  # and coords[0][0] > 50:
                image = image2[orig_h // 3 - 460 - 82: 2 * orig_h // 3 + h2 - 189, orig_w // 6 - 113: orig_w - 200, :]
                if self.VERBOSE:
                    print("remove bottom")

            orb = cv.ORB_create(nfeatures=25000)
            # get the keypoints and the corresponding descriptors
            kp1, des1 = orb.detectAndCompute(self.img_template, None)
            kp2, des2 = orb.detectAndCompute(image, None)

            bf = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True)
            # Match descriptors.
            matches = bf.match(des2, des1)  # query_image, train_image
            # Sort them in the order of their distance.
            matches = sorted(matches, key=lambda x: x.distance)
            if len(matches) > 120:
                matches = matches[0:len(matches) // 3]

            template_points = np.zeros((len(matches), 2), dtype=np.float32)
            query_points = np.zeros((len(matches), 2), dtype=np.float32)

            for i, m in enumerate(matches):
                template_points[i, :] = kp1[m.trainIdx].pt
                query_points[i, :] = kp2[m.queryIdx].pt
            if len(matches) == 0:
                if self.VERBOSE:
                    print("no match")
                continue
            if self.VERBOSE:
                print(len(matches), matches[0].distance, len(query_points), len(template_points))
            if len(query_points) < len(template_points) or len(query_points) == 0:
                if self.VERBOSE:
                    print("no qp")
                continue
            H, mask = cv.findHomography(query_points, template_points, cv.RANSAC)
            if self.VERBOSE:
                print(sum(mask))
            if (sum(mask) > 70 and len(matches) > 70 and matches[0].distance <= 15) or matches[0].distance < 16:
                break

        height, width, _ = self.img_template.shape  # the shape with respect to the template image
        warped_image = cv.warpPerspective(image, H, (width, height),
                                          flags=cv.INTER_NEAREST, borderMode=cv.BORDER_REPLICATE)
        return warped_image

    @staticmethod
    def compute_similarity(initial_image, header):
        image = cv.resize(initial_image, (0, 0), fx=0.25, fy=0.25)
        image = cv.cvtColor(image, cv.COLOR_RGB2GRAY)
        hi, wi = image.shape
        hh, wh = header.shape
        hs = min(hh, hi) - 2
        ws = min(wh, wi) - 2
        (score, diff) = structural_similarity(image[:hs, :ws], header[:hs, :ws], full=True)

        return score


def parse_args():
    parser = ArgumentParser()

    # Required parameters
    parser.add_argument("--task",
                        default=None,
                        type=int,
                        required=True,
                        choices=range(1, 4),
                        help="The number of the task (scenario) to be run. Task 4 is not implemented.")

    # Optional parameters
    parser.add_argument("--images_folder",
                        default='images/',
                        type=str,
                        help="Path to the folder containing the .jpg files. Default: images/")
    parser.add_argument("--labels_in_file_name",
                        default=False,
                        action='store_true',
                        help="Use this flag if each image file name contains the label"
                             " with the subject type and number")
    parser.add_argument("--output_folder",
                        default='results/',
                        type=str,
                        help="Path to the folder where output files will be written. Default: results/")
    parser.add_argument("--gt_folder",
                        default='ground-truth/',
                        type=str,
                        help="Path to the folder containing the .txt files with ground truth labels."
                             " Default: ground-truth/")
    parser.add_argument("--internal_folder",
                        default='internal/',
                        type=str,
                        help="Path to the folder containing internal files."
                             " Default: internal/")
    parser.add_argument("--needs_transform",
                        default=0,
                        type=int,
                        choices=range(3),
                        help="If input images are scanned or not."
                             " 0 (default) means autodetect - suitable for task 3"
                             " (it works for tasks 1 and 2, but it is slower)."
                             " 1 - no need to autodetect scene or transform the image - suitable for task 1."
                             " 2 - transform the image, but no need to autodetect the scene - suitable for task 2")
    parser.add_argument("--images_pattern",
                        default='*.jpg',
                        type=str,
                        help="Use this flag if there are several images in the IMAGES_FOLDER,"
                             " but not all of them are required."
                             " Default: *.jpg")
    parser.add_argument("--digit_pos",
                        default=0,
                        type=int,
                        help="Use this flag in conjunction with --images_pattern"
                             " to tell where the digit is after which underscore"
                             " in order to sort the files."
                             " Default: 0")
    parsed_args = parser.parse_args()
    return validate_args(parsed_args)


def validate_args(parsed_args):
    if parsed_args.needs_transform is 0:
        parsed_args.needs_transform = None
    elif parsed_args.needs_transform is 1:
        parsed_args.needs_transform = False
    elif parsed_args.needs_transform is 2:
        parsed_args.needs_transform = True

    if parsed_args.task == 1:
        if parsed_args.needs_transform is True:
            print("[WARNING] Task 1 does not need transforming the source image. Changing option to 1.")
            parsed_args.needs_transform = False
        elif parsed_args.needs_transform is None:
            print("[WARNING] Scene detection is enabled for Task 1. Consider setting --needs_transform to 1.")
        if not parsed_args.labels_in_file_name:
            print("[WARNING] Task 1 expects labels in file name."
                  " Consider adding --labels_in_file_name flag. Otherwise, labels will be automatically determined.")
    elif parsed_args.task == 2:
        if parsed_args.needs_transform is False:
            print("[WARNING] Task 2 needs transforming the source image. Changing option to 2.")
            parsed_args.needs_transform = True
        elif parsed_args.needs_transform is None:
            print("[WARNING] Scene detection is enabled for Task 2. Consider setting --needs_transform to 2.")
        if not parsed_args.labels_in_file_name:
            print("[WARNING] Task 2 expects labels in file name."
                  " Consider adding --labels_in_file_name flag. Otherwise, labels will be automatically determined.")
    elif parsed_args.task == 3:
        if parsed_args.needs_transform is not None:
            print("[WARNING] Task 3 needs to autodetect the scene. Changing option to 0.")
            parsed_args.needs_transform = None
        if parsed_args.labels_in_file_name:
            print("[WARNING] Task 3 does not expect labels in file name. Option ignored.")
            parsed_args.labels_in_file_name = False
    return parsed_args


if __name__ == "__main__":
    args = parse_args()
    # s = Solver(base_folder='../images/', gt_folder='../ground-truth', images_pattern='image_*.jpg')
    # s = Solver(images_folder='../additional_data/1.scanned/',
    #            gt_folder='../ground-truth',
    #            has_labels=False, labels_in_file_name=True, needs_transform=False)
    # s = Solver(base_folder='../additional_data/2.rotated+perspective/',
    #            gt_folder='../ground-truth',
    #            has_labels=False, labels_in_file_name=True, needs_transform=None)
    # s = Solver(base_folder='../additional_data/3.no_annotation/',
    #            gt_folder='../ground-truth',
    #            has_labels=False, labels_in_file_name=False, needs_transform=None)
    # s = Solver(base_folder='.', gt_folder='../ground-truth',
    #            has_labels=False, labels_in_file_name=True, needs_transform=True)
    # s = Solver(base_folder='../images/', gt_folder='../ground-truth',
    #            images_pattern='image_*.jpg', needs_transform=False, has_labels=True, labels_in_file_name=False)
    s = Solver(images_folder=args.images_folder, gt_folder=args.gt_folder, labels_in_file_name=args.labels_in_file_name,
               needs_transform=args.needs_transform, internal_folder=args.internal_folder,
               images_pattern=args.images_pattern, digit_pos=args.digit_pos)
    task_nr = args.task
    results, wrongs = s.solve()
    # [print(result) for result in results]
    # print(f"Accuracy: {((1 - wrongs / 150) * 100):.2f}%")

    os.makedirs(args.output_folder, exist_ok=True)
    with open(os.path.join(args.output_folder, f"micluta_campeanu_marius_407_task{task_nr}.txt"), "w") as out:
        out.writelines("\n".join(results))
        out.write("\n")
        out.flush()

# scanned
# (0.6765955481412373, 0.9609234561737854)
# (0.6156457027958082, 0.7839824629988419)
# (0.6486102251681263, 0.7577557924114423)

# rotated
# (0.4958362549754365, 0.5627671007946196)
# (0.431305111501315, 0.5385250309021595)
# (0.47666904936106513, 0.579787101253236)

# perspective
# (0.5933915146485101, 0.6571213924951455)
# (0.4710555764361241, 0.5560266298084846)
# (0.4941257295400597, 0.6251605561445245)

# we conclude that header2 differentiates the best
